Objective: I have learned what Refactoring is. At the same time, we did a lot of practice about these. We also completed teamwork and learned design pattern knowledge from other group presentations.
Reflective:  Fulfilled.
Interpretive: Refactoring can help me write more reasonable code and help me refactor other people's code in the future.
Decisional: I will try to use design patterns when writing project code, and use refactoring knowledge to write more reasonable code.