import java.util.*;

public class WordFrequencyGame {

    public static final String REGEX = "\\s+";

    public String getResult(String inputStr){

        final int ONE_WORD = 1;

        try {

            List<Input> inputList = countWords(inputStr, ONE_WORD);

            List<Input> list = getInputs(inputList);

            list.sort((preInput, nextInput) -> nextInput.getWordCount() - preInput.getWordCount());

            StringJoiner outputStr = new StringJoiner("\n");
            list.forEach(input -> outputStr.add(input.getValue() + " " +input.getWordCount()));
            return outputStr.toString();
        } catch (Exception e) {

            return "Calculate Error";
        }

    }

    private List<Input> getInputs(List<Input> inputList) {
        Map<String, List<Input>> map =getListMap(inputList);

        List<Input> list = new ArrayList<>();

        map.entrySet().stream().forEach(entry -> list.add(new Input(entry.getKey(), entry.getValue().size())));
        return list;
    }

    private List<Input> countWords(String inputStr, int ONE_WORD) {
        String[] inputStrArray = inputStr.split(REGEX);

        List<Input> inputList = new ArrayList<>();


        Arrays.stream(inputStrArray).forEach(inputString -> inputList.add(new Input(inputString, ONE_WORD)));
        return inputList;
    }


    private Map<String,List<Input>> getListMap(List<Input> inputList) {
        Map<String, List<Input>> map = new HashMap<>();
        inputList.forEach(input -> {
            if (!map.containsKey(input.getValue())){
                ArrayList arr = new ArrayList<>();
                arr.add(input);
                map.put(input.getValue(), arr);
            }

            else {
                map.get(input.getValue()).add(input);
            }
        });


        return map;
    }


}
